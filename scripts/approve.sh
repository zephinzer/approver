#!/bin/sh

REQUEST_ID=$1
HANDLED_BY=$2
COMMENT=$3

curl \
  -vv \
  -X PUT \
  --data "{\"comment\":\"$COMMENT\",\"approved\":true,\"handled_by\":\"$HANDLED_BY\"}" \
  "http://localhost:54321/requests/$REQUEST_ID";
