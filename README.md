# Approver

An approver service that stores an approval request for approval.

# Requirements

For development work you'll need the latest version of Go.

For integration/operations work you'll need the latest versions of the Docker Engine and Docker Compose.

# Usage

## Running the Demo

1. Run `make demo_start` to start the demo system
2. Run `./scripts/get_requests.sh` to retrieve pending requests
3. Run `./scripts/get_requests.sh | jq '.data.requests[].id' -r` to retrieve a list of request IDs
4. Choose a request ID (a UUIDv4) from the output of the above command
5. Run `./scripts/approve.sh ${REQUEST_ID} ${YOUR_NAME} "${ANY_COMMENTS}"` to approve it
6. Run `./scripts/reject.sh ${REQUEST_ID} ${YOUR_NAME} "${ANY_COMMENTS}"` to reject it
7. Run `make demo_stop` to end the demo system

## Development Notes

The repository is structured according to the layout highlighted at [Standard Go Project Layout](https://github.com/golang-standards/project-layout).

# Configuration

| Environment Variable | Description |
| --- | --- |
| `DB_HOST` | Hostname which database server is reachable at |
| `DB_PORT` | Port which database server is listening on |
| `DB_USER` | User to use to login to the database |
| `DB_PASSWORD` | Password of the user used to login to the database |
| `DB_DATABASE` | Name of the database schema to use |
| `Q_HOST` | Hostname where queue server is reachable at |
| `Q_PORT` | Port which queue server is listening on |
| `Q_TOKEN` | Secret token to authenticate with the queue |
| `Q_TOPIC` | Queue topic to subscribe to |
| `SERVER_HOST` | Hostname/IP address for service to bind to |
| `SERVER_PORT` | Port which service should listen on |

# License

Code is licensed under [the MIT license](./LICENSE).
