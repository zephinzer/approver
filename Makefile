PROJECT_NAME=approver
CMD_ROOT=approver
DOCKER_NAMESPACE=zephinzer
DOCKER_IMAGE_NAME=$(CMD_ROOT)
GIT_COMMIT=$$(git rev-parse --verify HEAD)
GIT_TAG=$$(git describe --tag $$(git rev-list --tags --max-count=1))
RUN_TIMESTAMP=$$(date +'%Y%m%d%H%M%S')

-include ./Makefile.properties

init:
	cd ./deploy && $(MAKE) init
denit:
	cd ./deploy && $(MAKE) denit
demo_start:
	cd ./deploy && $(MAKE) demo_start
demo_stop:
	cd ./deploy && $(MAKE) demo_stop
db_shell:
	mysql -uroot -ptoor -h127.0.0.1 -P3306 database
deps:
	go mod vendor -v
	go mod tidy -v
run:
	go run ./cmd/$(CMD_ROOT)
tests:
	go test ./...
build:
	go build -o ./bin/$(CMD_ROOT) ./cmd/$(CMD_ROOT)_${GOOS}_${GOARCH}
build_production:
	CGO_ENABLED=0 \
	go build \
		-a -v \
		-ldflags "-X main.Commit=$(GIT_COMMIT) \
			-X main.Version=$(GIT_TAG) \
			-X main.Timestamp=$(RUN_TIMESTAMP) \
			-extldflags 'static' \
			-s -w" \
		-o ./bin/$(CMD_ROOT)_$$(go env GOOS)_$$(go env GOARCH)${BIN_EXT} \
		./cmd/$(CMD_ROOT)
	rm -rf ./bin/$(CMD_ROOT)
	cp $$(pwd)/bin/$(CMD_ROOT)_$$(go env GOOS)_$$(go env GOARCH)${BIN_EXT} \
		./bin/$(CMD_ROOT)
images:
	@$(MAKE) image_approver
	@$(MAKE) image_loader
image_approver:
	docker build \
		--file ./deploy/build/approver/Dockerfile \
		--tag $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-server:latest \
		.
image_loader:
	docker build \
		--file ./deploy/build/loader/Dockerfile \
		--tag $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-loader:latest \
		.
save:
	mkdir -p ./build
	docker save --output ./build/approver.tar.gz $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-server:latest
	docker save --output ./build/loader.tar.gz $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-loader:latest
load:
	docker load --input ./build/approver.tar.gz
	docker load --input ./build/loader.tar.gz
publish_dockerhub:
	docker push $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-server:latest
	docker tag $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-server:latest \
		$(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-server:$(GIT_TAG)
	docker push $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-server:$(GIT_TAG)
	docker push $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-loader:latest
	docker tag $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-loader:latest \
		$(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-loader:$(GIT_TAG)
	docker push $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME)-loader:$(GIT_TAG)
see_ci:
	xdg-open https://gitlab.com/zephinzer/approver/pipelines
.ssh:
	mkdir -p ./.ssh
	ssh-keygen -t rsa -b 8192 -f ./.ssh/id_rsa -q -N ""
	cat ./.ssh/id_rsa | base64 -w 0 > ./.ssh/id_rsa.base64
