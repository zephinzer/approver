package main

import (
	"os"

	"gitlab.com/zephinzer/approver/internal/server"
)

func setupServer() {
	serverConfig := server.Config{
		Host: env.GetString("server_host"),
		Port: uint16(env.GetUint("server_port")),
	}
	log.Debugf("creating server instance that binds to '%s:%v'...", serverConfig.Host, serverConfig.Port)
	if err := server.Init(serverConfig); err != nil {
		log.Error(err)
		os.Exit(1)
	}
	log.Infof("created server instance that binds to '%s:%v'", serverConfig.Host, serverConfig.Port)
}
