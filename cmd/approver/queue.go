package main

import (
	"os"

	"gitlab.com/zephinzer/approver/internal/queue"
)

func setupQueue() {
	qConfig := queue.Config{
		Host:  env.GetString("q_host"),
		Port:  uint16(env.GetUint("q_port")),
		Token: env.GetString("q_token"),
	}
	log.Debugf("connecting to queue at '%s:%v'...", qConfig.Host, qConfig.Port)
	if err := queue.Init(qConfig); err != nil {
		log.Error(err)
		os.Exit(1)
	}
	log.Infof("connected to queue at '%s:%v'", qConfig.Host, qConfig.Port)
}
