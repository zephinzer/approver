package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/nats-io/nats.go"
	"github.com/spf13/cobra"

	"gitlab.com/zephinzer/approver/internal/queue"
	"gitlab.com/zephinzer/approver/internal/server"

	"gitlab.com/zephinzer/approver/internal/request"
)

var (
	Version   string
	Commit    string
	Timestamp string

	command *cobra.Command
)

func init() {
	initConfiguration()
}

func main() {
	command = &cobra.Command{
		Use:     "approver",
		Version: fmt.Sprintf("%s-%s / %s", Version, Commit, Timestamp),
		Short: strings.ReplaceAll(strings.Trim(`this service listens on a queue for incoming approval requests
		and inserts them into a database on reception. this service also
		starts a server with a restful interface which can be used by
		a front-end to approve/reject approval requests that have been
		picked up by this service
		`, "\n \t"), "\t", ""),
		PreRun: func(cmd *cobra.Command, args []string) {
			setupLogger()
			setupDatabase()
			setupQueue()
			setupServer()
		},
		Run: func(cmd *cobra.Command, args []string) {
			q := queue.Get()
			topic := env.GetString("q_topic")
			log.Infof("subscribing to queue topic '%s'", topic)
			var messageID uint64
			q.Subscribe(topic, func(msg *nats.Msg) {
				messageID++
				log.Tracef("[msg:%v] received message", messageID)
				var requestFromQueue request.Request
				log.Debugf("[msg:%v] parsing message", messageID)
				if err := json.Unmarshal(msg.Data, &requestFromQueue); err != nil {
					log.Errorf("[msg:%v] error parsing message: '%s'", messageID, err)
				}
				log.Debugf("[msg:%v] inserting request from service '%s' with id '%s' into database...", messageID, requestFromQueue.FromService, requestFromQueue.ID)
				if err := requestFromQueue.InsertIntoDatabase(); err != nil {
					log.Errorf("[msg:%v] error inserting into database: '%s'", messageID, err)
				}
				log.Infof("[msg:%v] inserted request from service '%s' with id '%s' into database", messageID, requestFromQueue.FromService, requestFromQueue.ID)
			})

			s := server.Get()
			log.Debugf("attempting to listen on '%s'", s.Addr)
			if err := s.ListenAndServe(); err != nil {
				panic(err)
			}
		},
	}

	command.Execute()
}
