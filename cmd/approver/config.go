package main

import (
	"github.com/spf13/viper"
	"gitlab.com/zephinzer/approver/internal/database"
	"gitlab.com/zephinzer/approver/internal/queue"
	"gitlab.com/zephinzer/approver/internal/server"
)

var env *viper.Viper

func initConfiguration() {
	env = viper.New()
	env.SetDefault("db_host", database.DefaultHost)
	env.SetDefault("db_port", database.DefaultPort)
	env.SetDefault("db_user", database.DefaultUser)
	env.SetDefault("db_password", database.DefaultPassword)
	env.SetDefault("db_database", database.DefaultDatabase)
	env.SetDefault("q_host", queue.DefaultHost)
	env.SetDefault("q_port", queue.DefaultPort)
	env.SetDefault("q_token", queue.DefaultToken)
	env.SetDefault("q_topic", queue.DefaultTopic)
	env.SetDefault("server_host", server.DefaultHost)
	env.SetDefault("server_port", server.DefaultPort)
	env.AutomaticEnv()
}
