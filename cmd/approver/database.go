package main

import (
	"os"

	"gitlab.com/zephinzer/approver/internal/database"
	"gitlab.com/zephinzer/approver/internal/request"
	"gitlab.com/zephinzer/approver/internal/response"
)

func setupDatabase() {
	dbConfig := database.Config{
		Host:     env.GetString("db_host"),
		Port:     uint16(env.GetUint("db_port")),
		User:     env.GetString("db_user"),
		Password: env.GetString("db_password"),
		Name:     env.GetString("db_database"),
	}
	log.Debugf("connecting to database using '%s@%s:%v'...", dbConfig.User, dbConfig.Host, dbConfig.Port)
	if err := database.Init(dbConfig); err != nil {
		log.Errorf("connection to database failed: '%s'", err)
		os.Exit(1)
	}
	log.Infof("connected to database using '%s@%s:%v'", dbConfig.User, dbConfig.Host, dbConfig.Port)

	log.Debugf("setting up database table to store requests...")
	if err := request.SetupDatabase(); err != nil {
		log.Errorf("setting up of `request` database table failed: '%s'", err)
		os.Exit(1)
	}
	log.Info("`request` database table set up")

	log.Infof("setting up database table to store responses...")
	if err := response.SetupDatabase(); err != nil {
		log.Errorf("setting up of `response` database table failed: '%s'", err)
		os.Exit(1)
	}
	log.Info("`response` database table set up")
}
