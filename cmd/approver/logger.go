package main

import (
	"github.com/usvc/logger"
	internalLogger "gitlab.com/zephinzer/approver/internal/logger"
)

var (
	log logger.Logger
)

func setupLogger() {
	internalLogger.Init()
	log = internalLogger.Get()
}
