package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"time"

	lorem "github.com/drhodes/golorem"
	"github.com/google/uuid"
	"github.com/nats-io/nats.go"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/usvc/logger"
	"gitlab.com/zephinzer/approver/internal/database"
	internalLogger "gitlab.com/zephinzer/approver/internal/logger"
	"gitlab.com/zephinzer/approver/internal/queue"
	"gitlab.com/zephinzer/approver/internal/request"
	"gitlab.com/zephinzer/approver/internal/response"
)

const DefaultServiceName = "loader"

var (
	Version   string
	Commit    string
	Timestamp string

	env     *viper.Viper
	command *cobra.Command
	log     logger.Logger
)

func run(cmd *cobra.Command, args []string) {
	serviceName := env.GetString("service_name")
	q := queue.Get()
	done := make(chan struct{})
	var incomingMessageCounter uint64
	q.Subscribe(serviceName, func(msg *nats.Msg) {
		incomingMessageCounter++
		var approvalResponse response.Response
		log.Tracef("[msg:%v] received response", incomingMessageCounter)
		if err := json.Unmarshal(msg.Data, &approvalResponse); err != nil {
			log.Errorf("[msg:%v] unable to parse incoming response with content body '%s': '%s'", incomingMessageCounter, string(msg.Data), err)
			return
		}
		if approvalResponse.Approved {
			log.Infof("[msg:%v] request with id '%s' was approved by response with id '%s'", incomingMessageCounter, approvalResponse.RequestID, approvalResponse.ID)
		} else {
			log.Warnf("[msg:%v] request with id '%s' was rejected by response with id '%s'", incomingMessageCounter, approvalResponse.RequestID, approvalResponse.ID)
		}
		log.Tracef("[msg:%v] completed processing request", incomingMessageCounter)
	})
	go func() {
		for {
			<-time.After(time.Duration(rand.Int63n(4000)) * time.Millisecond)
			fakedRequest := request.Request{
				ID:          uuid.New().String(),
				FromService: serviceName,
				Description: lorem.Paragraph(5, 10),
				CreatedAt:   time.Now(),
			}
			message, err := fakedRequest.JSON()
			if err != nil {
				log.Error(err)
				continue
			}
			q.Publish(env.GetString("q_topic"), []byte(message))
			log.Tracef("inserted request with id '%s' into queue in topic '%s'", fakedRequest.ID, queue.DefaultTopic)
		}
	}()
	<-done
}

func preRun(cmd *cobra.Command, args []string) {
	internalLogger.Init()
	log = internalLogger.Get()

	dbConfig := database.Config{
		Host:     env.GetString("db_host"),
		Port:     uint16(env.GetUint("db_port")),
		User:     env.GetString("db_user"),
		Password: env.GetString("db_password"),
		Name:     env.GetString("db_database"),
	}
	log.Debugf("connecting to database using '%s@%s:%v'...", dbConfig.User, dbConfig.Host, dbConfig.Port)
	if err := database.Init(dbConfig); err != nil {
		log.Error(err)
		os.Exit(1)
	}
	log.Infof("connected to database using '%s@%s:%v'", dbConfig.User, dbConfig.Host, dbConfig.Port)

	qConfig := queue.Config{
		Host:  env.GetString("q_host"),
		Port:  uint16(env.GetUint("q_port")),
		Token: env.GetString("q_token"),
	}
	log.Debugf("connecting to queue at '%s:%v'...", qConfig.Host, qConfig.Port)
	if err := queue.Init(qConfig); err != nil {
		log.Error(err)
		os.Exit(1)
	}
	log.Infof("connected to queue at '%s:%v'", qConfig.Host, qConfig.Port)
}

func init() {
	env = viper.New()
	env.SetDefault("db_host", database.DefaultHost)
	env.SetDefault("db_port", database.DefaultPort)
	env.SetDefault("db_user", database.DefaultUser)
	env.SetDefault("db_password", database.DefaultPassword)
	env.SetDefault("db_database", database.DefaultDatabase)
	env.SetDefault("q_host", queue.DefaultHost)
	env.SetDefault("q_port", queue.DefaultPort)
	env.SetDefault("q_token", queue.DefaultToken)
	env.SetDefault("q_topic", queue.DefaultTopic)
	env.SetDefault("service_name", DefaultServiceName)
	env.AutomaticEnv()

	command = &cobra.Command{
		Use:     "loader",
		Version: fmt.Sprintf("%s-%s / %s", Version, Commit, Timestamp),
		PreRun:  preRun,
		Run:     run,
	}
}

func main() {
	command.Execute()
}
