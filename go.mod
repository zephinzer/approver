module gitlab.com/zephinzer/approver

go 1.13

require (
	github.com/drhodes/golorem v0.0.0-20160418191928-ecccc744c2d9
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/nats-io/nats-server/v2 v2.1.4 // indirect
	github.com/nats-io/nats.go v1.9.1
	github.com/prometheus/client_golang v0.9.3
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.4.0
	github.com/usvc/db v0.0.2
	github.com/usvc/logger v0.1.3
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
)
