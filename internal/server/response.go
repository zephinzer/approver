package server

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Response struct {
	StatusCode int
	Message    *string     `json:"message"`
	Data       interface{} `json:"data"`
}

func (r *Response) Send(w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json; charset=utf8")
	output, err := json.Marshal(*r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf(`{"data":null,"message":"failed to serialize response: '%s'"}`, err.Error())))
		return
	}
	w.WriteHeader(r.StatusCode)
	w.Write(output)
}
