package server

import (
	"fmt"
	"net/http"

	"github.com/usvc/logger"
	internalLogger "gitlab.com/zephinzer/approver/internal/logger"
)

var log logger.Logger
var instance *http.Server

type Config struct {
	Host string
	Port uint16
}

func Init(config Config) error {
	log = internalLogger.Get()
	addr := fmt.Sprintf("%s:%v", config.Host, config.Port)
	handler := getRouter()
	handler = logIncomingRequests(handler)
	instance = &http.Server{
		Addr:    addr,
		Handler: handler,
	}
	return nil
}

func Get() *http.Server {
	if instance == nil {
		panic("server has not been initialised")
	}
	return instance
}
