package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/zephinzer/approver/internal/queue"
	"gitlab.com/zephinzer/approver/internal/request"
	"gitlab.com/zephinzer/approver/internal/response"
)

func handleGetPendingRequests(w http.ResponseWriter, r *http.Request) {
	requests := request.Requests{}
	requests.Load()
	respond(w, http.StatusOK, "ok", map[string]interface{}{
		"length":   len(requests),
		"requests": requests,
	})
}

func handleGetRequestByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	approvalRequest := request.Request{}
	err := approvalRequest.LoadFromDatabase(id)
	if err != nil {
		respond(w, http.StatusInternalServerError, fmt.Sprintf("failed to retrieve request with id '%s'", id), err)
		return
	} else if approvalRequest.ID == *new(string) {
		respond(w, http.StatusNotFound, fmt.Sprintf("request with id '%s' does not exist", id), err)
		return
	}
	respond(w, http.StatusOK, fmt.Sprintf("returning request with id '%s'", approvalRequest.ID), approvalRequest)
}

func handleUpdateRequestByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respond(w, http.StatusBadRequest, "failed to read request body", err)
		return
	}

	var approvalRequest request.Request
	if err = approvalRequest.LoadFromDatabase(id); err != nil {
		respond(w, http.StatusInternalServerError, fmt.Sprintf("failed to retrieve request with id '%s'", id), err)
		return
	} else if approvalRequest.ID == *new(string) {
		respond(w, http.StatusNotFound, fmt.Sprintf("request with id '%s' does not exist", id), err)
		return
	}

	var approvalResponse response.Response
	if err := approvalResponse.LoadFromRequestID(id); err != nil {
		respond(w, http.StatusInternalServerError, fmt.Sprintf("failed while attempting to retrieve repsonse for request with id '%s'", id), err)
		return
	} else if approvalResponse.ID != *new(string) {
		respond(w, http.StatusOK, fmt.Sprintf("request with id '%s' has already been responded to", id), map[string]interface{}{
			"request":  approvalRequest,
			"response": approvalResponse,
		})
		return
	} else if err = json.Unmarshal(body, &approvalResponse); err != nil {
		respond(w, http.StatusBadRequest, fmt.Sprintf("failed to parse request body: '%s'", string(body)), err)
		return
	}
	approvalResponse.ID = uuid.New().String()
	approvalResponse.RequestID = id
	approvalResponse.ToService = approvalRequest.FromService
	approvalResponse.CreatedAt = time.Now()
	if err = approvalResponse.InsertIntoDatabase(); err != nil {
		respond(w, http.StatusInternalServerError, "failed to insert response into database", err)
		return
	}

	approvalResponseMessage, err := approvalResponse.JSON()
	if err != nil {
		respond(w, http.StatusInternalServerError, "failed to retrieve json representation of approval response", err)
		return
	}
	if err = queue.Get().Publish(approvalResponse.ToService, []byte(approvalResponseMessage)); err != nil {
		respond(w, http.StatusInternalServerError, fmt.Sprintf("failed to publish response with id '%s' to the queue", approvalResponse.ID), err)
		return
	}
	respond(w, http.StatusOK, "ok", map[string]interface{}{
		"request":  approvalRequest,
		"response": approvalResponse,
	})
}
