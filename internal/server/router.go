package server

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func getRouter() http.Handler {
	router := mux.NewRouter()

	router.
		Handle("/metrics", promhttp.Handler()).
		Methods("GET")

	router.
		Handle("/requests", promhttp.InstrumentHandlerCounter(
			promauto.NewCounterVec(
				prometheus.CounterOpts{
					Name: "approver_query_get_request",
					Help: "number of times pending requests were retrieved",
				},
				[]string{"code"},
			),
			http.HandlerFunc(handleGetPendingRequests),
		)).
		Methods("GET")

	router.
		HandleFunc("/requests/{id}", promhttp.InstrumentHandlerCounter(
			promauto.NewCounterVec(
				prometheus.CounterOpts{
					Name: "approver_query_get_request_by_id",
					Help: "number of times requests were retrieved by id",
				},
				[]string{"code"},
			),
			http.HandlerFunc(handleGetRequestByID),
		)).
		Methods("GET")

	router.
		HandleFunc("/requests/{id}", promhttp.InstrumentHandlerCounter(
			promauto.NewCounterVec(
				prometheus.CounterOpts{
					Name: "approver_query_putt_request_by_id",
					Help: "number of times requests were updated by id",
				},
				[]string{"code"},
			),
			http.HandlerFunc(handleUpdateRequestByID),
		)).
		Methods("PUT")

	return router
}
