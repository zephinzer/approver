package server

const (
	DefaultHost string = "0.0.0.0"
	DefaultPort uint16 = 54321
)
