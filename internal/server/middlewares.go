package server

import (
	"net/http"
	"time"
)

func logIncomingRequests(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		then := time.Now()
		defer log.Tracef(
			"%s %s %s %s %s %s %v %vms",
			r.Proto,
			r.Method,
			r.RequestURI,
			r.Host,
			r.RemoteAddr,
			r.UserAgent(),
			r.ContentLength,
			time.Since(then).Milliseconds(),
		)
		next.ServeHTTP(w, r)
	})
}
