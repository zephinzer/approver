package server

import "net/http"

func respond(w http.ResponseWriter, status int, message string, data interface{}) {
	res := Response{
		StatusCode: status,
		Message:    &message,
		Data:       data,
	}
	if status >= 400 {
		log.Warn(message)
	} else {
		log.Debug(message)
	}
	res.Send(w)
}
