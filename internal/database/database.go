package database

import (
	"database/sql"

	"github.com/usvc/db"
)

var instance *sql.DB
var options db.Options

type Config struct {
	Host     string
	Port     uint16
	User     string
	Password string
	Name     string
}

func Init(config Config) error {
	options = db.Options{
		ConnectionName: "approver-service",
		Hostname:       config.Host,
		Port:           config.Port,
		Username:       config.User,
		Password:       config.Password,
		Database:       config.Name,
	}
	options.AssignDefaults()
	err := db.Init(options)
	if err != nil {
		return err
	}
	instance = db.Get(options.ConnectionName)
	return nil
}

func Get() *sql.DB {
	if instance == nil {
		panic("database has not been initialised")
	}
	return instance
}
