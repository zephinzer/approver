package database

const (
	DefaultHost     string = "127.0.0.1"
	DefaultPort     uint16 = 3306
	DefaultUser     string = "user"
	DefaultPassword string = "password"
	DefaultDatabase string = "database"
)
