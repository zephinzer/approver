package utils

import (
	"fmt"
	"time"
)

type RetryOptions struct {
	Function func() error
	Times    int
	Interval time.Duration
}

func retry(opts *RetryOptions) error {
	if opts.Function == nil {
		return fmt.Error(".Function was nil, this needs to be defined")
	} else if opts.Times <= 1 {
		return opts.Function()
	}
	<-time.After(opts.Interval)
	if err := opts.Function(); err != nil {
		opts.Times--
		return retry(opts)
	}
	return nil
}
