package logger

import (
	loggerLib "github.com/usvc/logger"
)

var log loggerLib.Logger

func Init() {
	log = loggerLib.New(loggerLib.Config{
		Format: loggerLib.FormatText,
	})
}

func Get() loggerLib.Logger {
	if log == nil {
		panic("logger has not been initialised")
	}
	return log
}
