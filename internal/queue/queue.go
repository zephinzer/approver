package queue

import (
	"fmt"

	nats "github.com/nats-io/nats.go"
)

var instance *nats.Conn

type Config struct {
	Host  string
	Port  uint16
	Token string
}

// Init initialises
func Init(config Config) error {
	natsURL := fmt.Sprintf("nats://%s:%v", config.Host, config.Port)
	connection, err := nats.Connect(natsURL, func(options *nats.Options) error {
		options.Token = config.Token
		return nil
	})
	if err != nil {
		return err
	}
	instance = connection
	return nil
}

func Get() *nats.Conn {
	if instance == nil {
		panic("queue has not been initialised")
	}
	return instance
}
