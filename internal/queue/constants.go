package queue

const (
	DefaultTopic string = "default_topic"
	DefaultHost  string = "127.0.0.1"
	DefaultPort  uint16 = 4222
	DefaultToken string = "token"
)
