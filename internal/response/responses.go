package response

import (
	"gitlab.com/zephinzer/approver/internal/database"
)

type Responses []Response

func (r Responses) Load() error {
	db := database.Get()
	stmt, err := db.Prepare("SELECT id, request_id, to_service, approved, handled_by, comment, created_at FROM request ORDER BY created_at")
	if err != nil {
		return err
	}
	rows, err := stmt.Query()
	if err != nil {
		return err
	}
	for rows.Next() {
		var res Response
		rows.Scan(
			&res.ID,
			&res.RequestID,
			&res.ToService,
			&res.Approved,
			&res.HandledBy,
			&res.Comment,
			&res.CreatedAt,
		)
		r = append(r, res)
	}
	return nil
}
