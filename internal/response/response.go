package response

import (
	"database/sql"
	"encoding/json"
	"time"

	"gitlab.com/zephinzer/approver/internal/database"
)

func SetupDatabase() error {
	db := database.Get()
	stmt, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS response (
			id VARCHAR(64) UNIQUE PRIMARY KEY NOT NULL,
			request_id VARCHAR(256) NOT NULL,
			to_service VARCHAR(128) NOT NULL,
			approved TINYINT(1) NOT NULL,
			handled_by VARCHAR(256) NOT NULL,
			comment TEXT,
			created_at TIMESTAMP DEFAULT NOW()
		) ENGINE=InnoDB;
	`)
	if err != nil {
		return err
	}
	_, err = stmt.Exec()
	if err != nil {
		return err
	}
	return nil
}

func NewFromJSON(input string) (*Response, error) {
	var res Response
	err := json.Unmarshal([]byte(input), &res)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

type Response struct {
	ID        string    `json:"id"`
	RequestID string    `json:"request_id"`
	ToService string    `json:"to_service"`
	Approved  bool      `json:"approved"`
	HandledBy string    `json:"handled_by"`
	Comment   string    `json:"comment"`
	CreatedAt time.Time `json:"created_at"`
}

func (r *Response) LoadFromRequestID(requestID string) error {
	db := database.Get()
	stmt, err := db.Prepare("SELECT id, request_id, to_service, approved, handled_by, comment, created_at FROM response WHERE request_id = ?")
	if err != nil {
		return err
	}
	row := stmt.QueryRow(requestID)
	if err := row.Scan(
		&r.ID,
		&r.RequestID,
		&r.ToService,
		&r.Approved,
		&r.HandledBy,
		&r.Comment,
		&r.CreatedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		return err
	}
	return nil
}

func (r Response) InsertIntoDatabase() error {
	db := database.Get()
	stmt, err := db.Prepare("INSERT INTO response (id, request_id, to_service, approved, handled_by, comment, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(r.ID, r.RequestID, r.ToService, r.Approved, r.HandledBy, r.Comment, r.CreatedAt)
	if err != nil {
		return err
	}
	return nil
}

func (r Response) JSON() (string, error) {
	output, err := json.Marshal(r)
	if err != nil {
		return "", err
	}
	return string(output), nil
}
