package request

import (
	"gitlab.com/zephinzer/approver/internal/database"
)

type Requests []Request

func (r *Requests) Load() error {
	db := database.Get()
	stmt, err := db.Prepare(`
		SELECT
			id,
			from_service,
			description,
			created_at
			FROM request
			WHERE id NOT IN (SELECT request_id FROM response)
			ORDER BY created_at
	`)
	if err != nil {
		return err
	}
	rows, err := stmt.Query()
	if err != nil {
		return err
	}
	for rows.Next() {
		var req Request
		rows.Scan(
			&req.ID,
			&req.FromService,
			&req.Description,
			&req.CreatedAt,
		)
		*r = append(*r, req)
	}
	return nil
}
