package request

import (
	"encoding/json"
	"time"

	"gitlab.com/zephinzer/approver/internal/database"
)

func SetupDatabase() error {
	db := database.Get()
	stmt, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS request (
			id VARCHAR(64) UNIQUE PRIMARY KEY NOT NULL,
			from_service VARCHAR(128) NOT NULL,
			description TEXT NOT NULL,
			created_at TIMESTAMP DEFAULT NOW()
		) ENGINE=InnoDB;
	`)
	if err != nil {
		return err
	}
	_, err = stmt.Exec()
	if err != nil {
		return err
	}
	return nil
}

func NewFromJSON(input string) (*Request, error) {
	var req Request
	err := json.Unmarshal([]byte(input), &req)
	if err != nil {
		return nil, err
	}
	return &req, nil
}

type Request struct {
	ID          string    `json:"id"`
	FromService string    `json:"from_service"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
}

func (r *Request) LoadFromDatabase(id string) error {
	db := database.Get()
	stmt, err := db.Prepare("SELECT id, from_service, description, created_at FROM request WHERE id = ?")
	if err != nil {
		return err
	}
	row := stmt.QueryRow(id)
	if err := row.Scan(
		&r.ID,
		&r.FromService,
		&r.Description,
		&r.CreatedAt,
	); err != nil {
		return err
	}
	return nil
}

func (r Request) InsertIntoDatabase() error {
	db := database.Get()
	stmt, err := db.Prepare("INSERT INTO request (id, from_service, description, created_at) VALUES (?, ?, ?, ?)")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(r.ID, r.FromService, r.Description, r.CreatedAt)
	if err != nil {
		return err
	}
	return nil
}

func (r Request) JSON() (string, error) {
	output, err := json.Marshal(r)
	if err != nil {
		return "", err
	}
	return string(output), nil
}
